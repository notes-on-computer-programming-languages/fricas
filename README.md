# fricas

Computer algebra system. http://fricas.sourceforge.net

* http://aldor.org
* [*7 Graphics*
  ](http://wiki.fricas.org/uploads/chapter-7.xhtml)

## Unofficial documentation
* [*FriCAS*](https://en.m.wikipedia.org/wiki/FriCAS)
 
## Binary packages availability
* [Repology](https://repology.org/project/fricas/versions)
* [Docker](https://hub.docker.com/search?q=fricas&type=image)

As few distribution make it available yet, one may have to use its ancestor
project Axiom
* [DebianScienceComputerAlgebraSystems](https://wiki.debian.org/DebianScience/ComputerAlgebraSystems)
* [Debian RFP: fricas -- computer algebra system](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=890525) 2018
* https://repology.org/project/axiom

## See also
 * [List of computer algebra systems](https://en.m.wikipedia.org/wiki/List_of_computer_algebra_systems)